#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>
#include <ArduinoJson.h>

#include <Adafruit_LSM6DS3TRC.h>

// For SPI mode, we need a CS pin
#define LSM_CS 10
// For software-SPI mode we need SCK/MOSI/MISO pins
#define LSM_SCK 13
#define LSM_MISO 12
#define LSM_MOSI 11

const int buttonPin1 = D0;
const int buttonPin2 = D1;
const int buttonPin3 = D2;

int buttonState1 = 0;
int buttonState2 = 0;
int buttonState3 = 0;
int lastButtonState1 = 0;
int lastButtonState2 = 0;
int lastButtonState3 = 0;

unsigned long debounceDelay = 50;  
unsigned long lastDebounceTime1 = 0;  // Time of last state change for button 1
unsigned long lastDebounceTime2 = 0;  // Time of last state change for button 2
unsigned long lastDebounceTime3 = 0;  // Time of last state change for button 3

Adafruit_LSM6DS3TRC lsm6ds3trc;

// Some variables we will need along the way
const char* ssid     = "your_wifi_name";
const char* password = "Your_wifi_pass"; 
const char* PARAM_MESSAGE = "message"; 
int webServerPort = 80;

String y_axis = "still";
String x_axis = "still";
String color = "red";
String color_array[5] = { "red", "blue", "green", "purple", "black" };
String thickness = "0";
int button1_pushed = 0;
int button2_pushed = 0;
int button3_pushed = 0;


// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {

  pinMode(buttonPin1, INPUT_PULLUP);
  pinMode(buttonPin2, INPUT_PULLUP);
  pinMode(buttonPin3, INPUT_PULLUP);

  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  if (!lsm6ds3trc.begin_I2C()) {
    // if (!lsm6ds3trc.begin_SPI(LSM_CS)) {
    // if (!lsm6ds3trc.begin_SPI(LSM_CS, LSM_SCK, LSM_MISO, LSM_MOSI)) {
    Serial.println("Failed to find LSM6DS3TR-C chip");
    while (1) {
      delay(10);
    }
  }

  Serial.println("LSM6DS3TR-C Found!");
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", "Hello world!");
    response->addHeader("Access-Control-Allow-Origin", "*");
    request->send(response);
  });

  server.on("/params", HTTP_GET, [](AsyncWebServerRequest *request){
    String json = "{\"y_axis\":\"" + y_axis + 
    "\", \"x_axis\":\"" + x_axis + 
    "\", \"color\":\"" + color + 
    "\", \"thickness\":\"" + thickness + 
    "\", \"reset\":\"" + String(button3_pushed) + 
    "\"}";
    AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", json.c_str());
    response->addHeader("Access-Control-Allow-Origin", "*");
    request->send(response);
    button3_pushed = 0;
  });

    // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  lsm6ds3trc.configInt1(false, false, true); // accelerometer DRDY on INT1
  lsm6ds3trc.configInt2(false, true, false); // gyro DRDY on INT2

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();

}

void loop() {
  // Read the current state of button 1
  int reading1 = digitalRead(buttonPin1);

  // Check if button 1 state has changed
  if (reading1 != lastButtonState1) {
    lastDebounceTime1 = millis();  // Record the time of state change for button 1
  }

  // Check if enough time has passed since the last state change of button 1
  if ((millis() - lastDebounceTime1) > debounceDelay) {
    if (reading1 != buttonState1) {
      buttonState1 = reading1;

      if (buttonState1 == LOW) {
        button1_pushed += 1;
        color = color_array[button1_pushed%5];
      }
    }
  }

  // Update the last state of button 1
  lastButtonState1 = reading1;

  // Read the current state of button 2
  int reading2 = digitalRead(buttonPin2);

  // Check if button 2 state has changed
  if (reading2 != lastButtonState2) {
    lastDebounceTime2 = millis();  // Record the time of state change for button 2
  }

  // Check if enough time has passed since the last state change of button 2
  if ((millis() - lastDebounceTime2) > debounceDelay) {
    if (reading2 != buttonState2) {
      buttonState2 = reading2;

      if (buttonState2 == LOW) {
        button2_pushed += 1;
        thickness = String(button2_pushed%5);
      }
    }
  }

  // Update the last state of button 2
  lastButtonState2 = reading2;

  // Read the current state of button 3
  int reading3 = digitalRead(buttonPin3);

  // Check if button 3 state has changed
  if (reading3 != lastButtonState3) {
    lastDebounceTime3 = millis();  // Record the time of state change for button 3
  }

  // Check if enough time has passed since the last state change of button 3
  if ((millis() - lastDebounceTime3) > debounceDelay) {
    if (reading3 != buttonState3) {
      buttonState3 = reading3;
if (buttonState3 == LOW) {
        button3_pushed = 1;
      }
    }
  }

  // Update the last state of button 3
  lastButtonState3 = reading3;
//
//  buttonState1 = digitalRead(buttonPin1);
//  buttonState2 = digitalRead(buttonPin2);
//  buttonState3 = digitalRead(buttonPin3);
//
//  if (buttonState1 == LOW) {
//    button1_pushed += 1;
//    color = color_array[buttonPin1%5];
//    Serial.println("button pushed");
//  }
//  if (buttonState2 == LOW) {
//    button2_pushed += 1;
//    thickness = String(button2_pushed%5);
//  }
//  if (buttonState3 == LOW) {
//    button3_pushed = 1;
//  }

  sensors_event_t accel;
  sensors_event_t gyro;
  sensors_event_t temp;
  lsm6ds3trc.getEvent(&accel, &gyro, &temp);

  y_axis = "still";
  x_axis = "still";
  
  if (accel.acceleration.x > 0.5) {
    y_axis = "up";
  } 
  if (accel.acceleration.x > 2.5) {
    y_axis = "upup";
  }

  if (accel.acceleration.x < -0.5) {
    y_axis = "down";
  } 
  if (accel.acceleration.x < -2.5) {
    y_axis = "downdown";
  }
  
  if (accel.acceleration.y > 0.5) {
    x_axis = "up";
  }
  if (accel.acceleration.y > 2.5) {
     x_axis = "upup";
  }
  if (accel.acceleration.y < -0.5) {
    x_axis = "down";
  }
  if (accel.acceleration.y < -2.5) {
     x_axis = "downdown";
  }

  
  delay(10);
}
