---
title: "\nWeek 2: Hugo static site generator"

---

This week we focused mainly on two things. First was to make out website more presentable with Hugo static site generator. I haven't worked with [Hugo](https://gohugo.io/) before, so it was an interesting experience. It was nice to choose from variety of themes, the only thing I struggled with were the default icons. The second thing was the Final Project idea, which I haven't thought much yet. Thinking of making a "smarter" furniture perhaps.

I modified the Zen theme quite a bit. At the landing page, there are the previews of the documentation pages. However, it was not really clear that these are only previews, and the reader of my website could have been misleading. I have therefore overwritten the `summary.html` page (copied the one that was in the yen theme, paste it in the `/layouts/_default/` and made the modifications), and added `Read more` link at the end of it. More about how to modify a Hugo theme: https://gohugobrasil.netlify.app/themes/customizing/

I changed to copyright to be my name. In the original theme setup, the copyright was the website name, so Digital Fabrication in this case (I assume the theme is mainly used to create some personal sites or blogs). This was possible by overwriting the `baseof.html` (similar to the procedure above with the `summary.html`).

The default icons were changed to reflect this course (Digital Fabrication), so now we have a cute 3D printer as a main icon! The icons used are made by Hans Gerhard Meier, and are appropriately credited in the footer of the website.

Note: you can use `shortcodes` to make more sophisticated changed to Markdown, for example to change the way hoe the tables are shown etc. Some built in shortcodes include Figure (to include images) or Highlight (nice code highlighting).

### Including images in the posts

The simplest way to include image in the post is to use 
```
![Image caption](/digital-fabrication/week2/3d_printer.jpeg)
```
And it will be shown like this:

![Image caption](/digital-fabrication/week2/3d_printer.jpeg)

The image caption will be shown when the image cannot be found or loaded, and it will look like this:

![Image caption](/digital-fabrication/week2/4d_printer.jpeg)

The more clever way to include image in the post is to use the figure shortcode. It can take a number of arguments (such as title or height|as well and therefore can make the image appear nicer to the website. The complete list of arguments can be found here, together with explanation what shortcodes are: https://gohugo.io/content-management/shortcodes/ 

{&lbrace;< figure src="week2/3d_printer.jpeg" caption="**Figure 1:** this is a 3D printer" height="450">&rbrace;}

And it will look like this:

{{< figure src="week2/3d_printer.jpeg" caption="**Figure 1:** this is a 3D printer" height="450">}}

However, what if you want to have 2 images next to each other? Or perhaps have the images in the middle of the page not aligned in the left? The proper way would be to modify the shortcode to allow these options, or to write a new one. However, because this is not a production web, just a class project, we can use a bit of a heck and insert out images into a table.
The markdown looks like this:

{{< figure src="week2/screenshot.jpg" height="450">}}

And on the website it will look like this:
|    |    |    |
|----------|----------|----------|
|{{< figure src="week2/3d_printer.jpeg" caption="**Figure 1:** this is a 3D printer" height="450">}}| &nbsp; &nbsp; |{{< figure src="week2/laser_cutter.jpeg" caption="**Figure 2:** this is a laser cutter" height="450">}}

### Spell checker
During the course, I will be writing a lot of documentation. I would like to do it directly in the VS Code, and to prevent spelling mistakes, I installed a [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker) extension.

## Sharing files
I created a folder called "files" in my main content folder. Now I can share files with the following command:
```
Example File - [TXT file](/digital-fabrication/files/example_file.txt)
```
If you want your file to automatically start downloading, compress it to a .zip file.