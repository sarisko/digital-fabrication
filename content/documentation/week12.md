---
title: "Week 12: Machine Building"

---

The task for this week was to build a machine - something that can be numerically controlled.
We decided to order a Prusa Mini+ kits, so I built a 3D printer! 

{{< youtube OCcxhQrwnko >}}

There was a very (long) helpful assembly guide. I particularly liked that each step could be commented by the community, e.g. all the people that were also building the printer. In that way there was an additional support for all the steps, many people posted how they managed to complete each steps. Very useful!

There were few parts about the build that were particularly challenging (at least for me), for example fitting all the electronics to the box or the arrangement of the cables. The assembly mentioned a [screw pulling technique](https://youtu.be/T29H8PE5BSc), which was impressive - will use it in future for sure.

The final thing I can do is to compare building the Prusa Mini+ printer (from kit) to building Ender 3v2:

***Prusa Pros:*** Support of the community, very easy first print (calibration) setup after the printer is assembled.

***Prusa Cons:*** The build indeed took a very long time and I couldn't make the printer use Cura (they have their own [slicer](https://www.prusa3d.com/page/prusaslicer_424/)).

***Ender Pros:*** Very useful video assembly guide, a bit easier kit -> less trouble with electronics, and Cura slicer works.

***Ender Cons:*** Nightmare of calibration (at least for a first timer).