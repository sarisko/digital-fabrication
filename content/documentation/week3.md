---
title: "\nWeek 3: Project Management with Git"

---
This page contains the guides of how I created and managed the documenting website for the Digital Fabrication course.

### Tools used
- [GitLab](https://gitlab.com/)
- [Python](https://www.python.org/)
- [Hugo](https://gohugo.io/)
- [Markdown](https://www.markdownguide.org/) ([useful documentation](https://www.markdownguide.org/basic-syntax))
- [Homebrew](https://brew.sh/)

## Creating website

I decided to use Hugo as my website will be static (mostly just documentation). First we need to download Hugo software, I am using brew as I have a MacBook. 
```
brew install hugo
```
Then in the directory I wanted to initialise the project I ran:
```
hugo new site digital-fabrication
cd digital-fabrication
```
After that I initialised the repository which will be later pushed to [GitLab](https://gitlab.com/sarisko/digital-fabrication/).
```
git init
```
Another reason for me to use the Hugo was because of the variety of themes it has. I am using the [Zen](https://themes.gohugo.io/themes/hugo-theme-zen/) theme, which I modified to look more to my liking.
```
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke themes/ananke
echo "theme = 'ananke'" >> config.toml
```
To locally run the page we just made:
```
hugo server
```

While the server is running, the default page where your site is running is http://localhost:1313/digital-fabrication/ so if you open that page in your browser, you will see how it currently looks like. 

The next steps for me were to modify the website. I reused the default structure, and changed the content. I also tried to change the icons and some default elements.

One thing I was happy about was using a VS Code extension that helps you check the grammar in the Markdown files. Link to the extension -> https://marketplace.visualstudio.com/items?itemName=valentjn.vscode-ltex

## Using Git and GitLab

When I was happy with the changes I made, I wanted to transfer these changes to my remote GitLab repository hosted at https://gitlab.com/sarisko/digital-fabrication.

### SSH 
Public and private key is a for of authentication (like a username and password). However, in this case, there is a private key that only you know, and a public key that can everyone know. It works something like this (my explanation is simpler version of what's happening): You create a public and private key pair on your machine, and give your GitLab account the public key. So the GitLab knows that the public key is legit. Now it can encrypt the data using it, and __only the private key__ (on your machine) will be able to decrypt it. It is impossible to know what the private key is only knowing the public one.

So in practice, the next command will create a private & public key pair in the current directory, so we need to make sure we are in the `~/.ssh`.
```
ssh-keygen -t rsa -b 4096 -f lab_rsa
```
Here the `-t` stands for type, `b` for byte (length of the key) and `-f` for filename. It will create two files: `lab_rsa` and `lab_rsa.pub`. 
Note that `lab_rsa` is a secret! Handle with care, don't copy or `cat` it. If you ever reveal it, create a new key pair.

However, `lab_rsa.pub` can be handled as a publicly known information, so you can copy it and paste it to you GitLab repository.

Now we can ssh to our remote repository and push any changes there.

Note: It is also nice to modify the config file in the `.ssh` directory.

More information about the whole process and how the SSH works: https://docs.gitlab.com/ee/user/ssh.html

### Transferring changes to remote repo

At first, I like to look at status of my local repository. We use local command line for the next few steps.
```
git status
```
With this command I can check the new files I created, and all the files I modified. So in the next step I can add all the files I want to "transfer" to the remote repository. 

```
git add .
```
The above command includes all the changes. I usually want to include everything I changed (the files I don't are usually included in the .gitignore file, which will be mentioned later).

Now if I want to commit the changes, I use the commit command.
```
git commit -m "Message about what was changed"
```
It will create a commit with a message (using -m flag for that).

Now it's time to push the commit to the remote repository.
```
git push
```

And it's done! Because we made a `.yml` file, the GitLab will run a pipeline and publish the changes to my website - https://sarisko.gitlab.io/digital-fabrication/. The `.yml` file was a template from Hugo, but we can also write our own `.yml` if we want to publish our website somewhere else.

The `gitlab-ci.yml` file:
```yml
image: registry.gitlab.com/pages/hugo/hugo_extended:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

I'm using only the main branch, because there is no collaboration happening, so some excessive branching is not necessary.

As mentioned above, I also created a `.gitignore` file. There are some files (usually some machine related config) that are not necessary to have at the remote repo. So that we don't have to avoid adding them to our commit, we can simply write the name of the file/folder to `.gitignore` file, and as the filename suggests, the git will then ignore this file and any changes in it from now on.

## Optimizing images and videos
Make sure you use jpeg over png. For conversion, I am using either https://squoosh.app/ or magick library. On MacBook, can be installed with:
```
brew install imagemagick
```
Converting:
```
magick image_name.png image_name.jpg
```
Converting all the files with specific file type ([more info here](https://dototot.com/imagemagick-tutorial-batch-resize-images-command-line/)):
```
magick mogrify -format jpg *.png
```
Reducing the size:
```
magick convert image_name.jpg -resize 1200x630 image_name_1200x630.jpg
```
Reducing the quality of the image:
```
magick convert image_name.jpg -quality 65 image_name_1200x630_65.jpg
```
It can all be combined in one command.

Note: For videos, it is advised to use FFmpeg - https://ffmpeg.org/ and `brew install ffmpeg`

## Reflection
This week I mostly spend documenting my progress. As I used all the tools before, my workflow was smooth, so I documented all that I found interested rather than things I was stuck on. I found the talk about the image and video optimisation interesting. I agree that it is important to keep your repositories small. Looking forward to the next week!

