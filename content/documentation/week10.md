---
title: "Week 10: Electronic Production"

---
## Introduction
We learned how to do the PSB milling. We started by generating a toolpaths, then preparing the Roland SRM-20, and after the milling was done, soldering the finished board. This was the first week when I had multiple things go wrong (at least 3 unsuccessfully milled board), but I also learned a lot, and now I make PCBs quite often.

## Preparing the files

From the electronic design week, we got the Gerber files, ideally all layers in the single file, and optionally some drill files.

To create the toolpaths for the Roland machine, we will use the CopperCAM software which is installed on the computer next to the PCB milling machine.

{{< figure src="week10/interface.jpg" caption="**Figure 1:** The CopperCAM program" height="450">}}

When I want to make a PCB, I like to follow the Fablab [Wiki page](https://wiki.aalto.fi/display/AF/PCB+Milling+with+Roland+SRM-20), where everything is clearly explained, just to make sure I don't miss anything. Here, I will outline important steps and add some bits that I found interesting / useful.

**1.** Import the Gerber file to CopperCAM -> make sure that the edge cut is highlighted

**2.** In the File -> Dimensions, margin should be 1 mm, and Z thickness a little more than your board. Then go to File -> Origin, and change it to 0 and 0 (usually Y is something like 0.05 for some reason, not sure why)

**3.** I usually don't use drill files, but rather select the pad where I would like to have a hole. Then right click on it and edit it (or all identical). This will give me some options (Figure 4), one of them being "drill". I like using rivets, so I usually make the holes bigger than rivers (for example for the 1.4 mm rivets I make the holes 1.5 mm)

|    |    |    |
|----------|----------|----------|
|{{< figure src="week10/all_identical.jpg" caption="**Figure 2:** Right-click on a pad" height="450">}}| &nbsp; &nbsp; |{{< figure src="week10/all_identocal2.jpg" caption="**Figure 3:** It highlights all pads" height="450">}}

{{< figure src="week10/custom_holes.jpg" caption="**Figure 4:** Edit pad options" height="450">}}

**4.** The next thing is to set up the tools we use. I just go with the defaults, e.g. what is written in the Wiki. 
There are three types on tools
- Engraving (for making the paths)
- Cutting (for cutting out the board, edge cuts)
- Drilling (for headpins or pins, min 0.8 mm diameter)

I usually only use two, one for engraving (Tool 1) and one for Drilling and Cutting (Tool 2).

{{< figure src="week10/wiki_tools.jpeg" caption="**Figure 5:** Tool Setting (source [FabLab wiki](https://wiki.aalto.fi/display/AF/PCB+Milling+with+Roland+SRM-20))" height="450">}}

{{< figure src="week10/milling_bits.jpg" caption="**Figure 6:** Brown and Purple is engraving, Orange for drilling and cutting" height="450">}}

**5.** Sometimes when I have a text in my design, at this point I right-click at one of the elements of text, and press "Engrave tacks as centerlines". Some parts of the text might not work (for example the tilt in the letter "t" never works for me). I guess it's ok, but then don't forget to disable it before next step, or otherwise there will be contours around it.

**6.** The next step is to calculate the contours (the button can be seen on Figure 1). There are two numbers to enter --> numbers of successive contours (how many rounds will the tip mill around the tracks and pads, usually around 3 is enough) and extra contours around pads (I usually just put 0 there).

**7.** The last step is to export the file. Select the Mill icon (the button can be seen on Figure 1). Engraving layer, drilling and then cutting out in this order. XY-zero should be at South west corner, and Z-zero at circuit surface. After saving, we usually get two files (named something with T(ool)1 and T(ool)2 -> one for engraving and the second one for drilling and cutting) 

Note: It is possible to use the double-sided PCB milling, but I haven't tried that yet.

{{< figure src="week10/settings1.jpg" caption="**Figure 7:** Export the toolpath" height="450">}}

## Preparing the Roland machine

Before we start milling, let's prepare everything. First lets sand the copper clad a bit with the brass coil "sponge" up until its nice and shiny. We do it because (according to ChatGPT) it enhanced adhesion, removes contamination and promotes uniform coating.

The copper clad would move if we don't attach it to the bed, so we use double-sided tape for that.

{{< figure src="week10/tape.jpg" caption="**Figure 8:** Tape wrong" height="450">}}

From my experience the take should cover the metal sheet evenly (**not** like on the Figure above), otherwise it might unstick during the print, which happened to me as well. Now I neatly take the whole metal sheet before milling, so it sticks nicely.

After that we should insert the correct tooltip. First engraving tip (currently it's the brown now, as seen on Figure 6), then drilling and cutting tip (orange one on Figure 6).

The last thing to do is to set the origin point for XYZ axis. For X and Y, you can just move the tooltip where you would like the bottom left corner of your design to start, and set that as the origin in the Roland milling machine interface - make sure that there is enough space, and leave at lest 0.8 mm from edges. I usually like to go to CopperCAM and point my mouse to the top right corner of my design. Then on the bottom panel you can see the dimensions of your PCB.

Next is the setting the origin for Z-axis. The tip should be inserted all the way in, move it close to the blank PCB sheet, and stop about 5 mm from it (be careful not to touch the metal sheet as it might break the tip! There is a setting to move the tool slower). The release it and (gently) let it touch the sheet - you can then screw it back, and reset the Z-axis. I recommend setting this in the top half of your PCB design (usually in the middle, but the PCB milling machine we have the lab is a bit uneven).

Now we can start the cutting --> Cut -> delete everything -> choose your file (first tool 1, then tool 2). Sometimes you have to change to look for all files.

{{< figure src="week10/roland_software.jpg" caption="**Figure 9:** PCB milling machine interface" height="450">}}

After you change the tool to do the second part of the milling (drilling and engraving), set the Z-axis origin point again, but **leave the XY as it is**.
## PCB Milling

Now it's the milling time! Juts to have an estimate, it takes around 20min to mill, but depends a lot on how complicated your board is. Honestly at first I had sooo many problems with it. It was before the bed was leveled again (after I returned I didn't have that many problems). The first mill was too shallow, and then the second one too deep.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week10/fail_milling.jpg" caption="**Figure 10:** Shallow cut" height="450">}}| &nbsp; &nbsp; |{{< figure src="week10/milling1.jpg" caption="**Figure 11:** Deep cut" height="450">}}

{{< figure src="week10/two_extremes.png" caption="**Figure 12:** Three failed mills - everything is wrong" height="450">}}

The two videos bellow: in the right one the cut is too shallow, and therefore if fails to scrape the conductive layer (everything is then connected to everything). In the left video I think the cut is too deep (and I perhaps had a wrong file where the traces were too thin, not 0.4 mm as we are supposed to be set up in the KiCAD, but the default 0.2 mm), and there were no traces between some components.

|    |    |    |
|----------|----------|----------|
|{{< video src="week10/too_little_milling.mp4" width="400" >}}| &nbsp; &nbsp; |{{< video src="week10/too_much_milling.mp4" width="400" >}}

{{< figure src="week10/edge_cuts.jpg" caption="**Figure 13:** Edge cuts with tool" height="450">}}

At the end of the day, I managed to get some working PCBs. The ideal scenario is that the traces are connecting only the components they are supposed to be connecting.
There were two big learning:
- the top right corner of the bed works the best, the rest is quite troublesome
- for bigger PCBs use the bigger CNC machine

{{< figure src="week10/proper_pcb.jpg" caption="**Figure 14:** Nice looking PCB" height="450">}}

## Other problem

When we run out of singe sided sheets, I just used the double-sided one, and put holes for rivers in it - big mistake!. Had to scrape it, and even then it worked meh. Need to be more careful next time.

{{< figure src="week10/ruined_pcb.jpg" caption="**Figure 15:** Improvisation with double-sided sheet" height="450">}}
## Soldering and Testing

The voltmeter became my best friend - checking whether my soldering works, whether there is a short circuit etc. I also started to like the rivets - connecting the top of the board with bottom, or just soldering it on the bottom. Honestly though, it also happened that many times they did not work as expected. 

|    |    |    |
|----------|----------|----------|
|{{< figure src="week10/rivets.jpg" caption="**Figure 16:** Rivets" height="450">}}| &nbsp; &nbsp; |{{< figure src="week10/rivet_punching.jpg" caption="**Figure 17:** Rivet punching machine" height="450">}}

There is also a two-sided rivet punch tools (used with hammer).

|    |    |    |
|----------|----------|----------|
|{{< figure src="week10/rivets_back.jpg" caption="**Figure 18:** Rivets bottom" height="450">}}| &nbsp; &nbsp; |{{< figure src="week10/rivets_front.jpg" caption="**Figure 19:** Rivets top" height="450">}}

Then I saw that Saskia did --> she put the XIAO from the bottom, and soldered it on top (Figure 19). Of course one needs to be careful and mirror the pins when making the design. It looked quite nice, and easy to solder. I should try that the next time.

{{< figure src="week10/wrongSide.jpeg" caption="**Figure 20:** Source: [Saskias FabAcademy page](https://fabacademy.org/2023/labs/aalto/students/saskia-helinska/assignments/electronicsproduction.html)" height="450">}}

I also learned a lot about soldering, especially about soldering tiny components. During my first try, I wanted to make everything at once, and then I expected it would work just like that. But it's not that not simple, and it is also impossible to debug afterwards. I learned my lesson when I was looking for my mistake (short circuit) for solid 20 min. I even removed the component (Figure 21). 

|    |    |    |
|----------|----------|----------|
|{{< figure src="week10/ruined_solder.jpg" caption="**Figure 21:** Something was wrong, and I couldn't figure out where" height="450">}}| &nbsp; &nbsp; |{{< figure src="week10/zoom_solder.jpg" caption="**Figure 22:** Zoom up of my messed up soldering" height="450">}}

What I do now (in addition to soldering one pad at a time) is that I put a bit of solder to a pad fist. Then I put some component on top of it, using tweezers. Now when I meld the solder, the component will just nicely sink in to it and hold perfectly.

## Final words

During this week, I made a few boards, but none of them was successful one. But I learned a lot from it, and then made a good boards, such as "The pig" and "Angle lighter".

{{< video src="week11/final_pig.mp4" width="400">}}

{{< video src="week13/board_led.mp4"  >}}

<!-- till need to send them gerber files -->

<!-- 
### Todo:
- allign the title better
- SLA documentation, try it out
- recheck if I want to include more links
- mesh lab - fixing 3d onject before 3d printing
- the program that can make a beautiful sketch from fusion
- change about me picture to something smaller
- how wide is the web depending on how big the window is -->
