---
title: "\nWeek 4: Computer-Aided Design"

---
This week we started to look at 3D modelling. The aim was to try different modelling tools such as Fusion360 or FreeCAD. 

It was interesting to hear that we should try to have the sketches fully constrained at the end of sketching/modelling. I was aware of the parametrisation feature in Fusion and used it a lot, but never noticed the degrees of freedom.

## Fusion 360
Fusion is the software I am the most Familiar with since I design the syringe pump and some other things such as this hexagonal pot (Figure 1) in it.

{{< figure src="week4/fusion.jpg" caption="**Figure 1:** Fusion Project" height="450">}}

Therefore, I'll not be using it this week :D

## Blender
At the beginning I tried working with Blender. I heard a lot about it, but it always seemed as a very unintuitive tool where you can find everything if you know where to look (but I don't). After two hours of struggle I felt that this is not what I want to be doing, and would rather try something else. I do believe it can be a wonderful program if I'm introduced to it properly, but right now I just don't like it.

Note: I heard that you can be programming in Blender (there is a Blender Python API) so now I am a bit more exciting about it! I hope I'll get to use it sometime.

## OpenSCAD

When I first heard about this software, I was intrigued because you use programming to model something in 3D. There is no need to have sketches since you can directly model the 3D object.

This is how the OpenSCAD looks like when you open the new project:
{{< figure src="week4/openscad.jpg" caption="**Figure 2:** OpenSCAD blank project" height="450">}}

I did some tutorials and made some stuff, but I'm not an expert. Everything I'll write here is just my findings and perceptions.
When I started using OpenSCAD, the most important commands were cube, translate and rotate. Cube (or cylinder, or something else that creates shape for you) will create the object on your current position, translate moves your position on the plane, and rotate will rotate the object you are about to create.

```
x = 2;
y = 4;
z = 8;
translate([x,y,z])
    rotate([90,0,0])
        cylinder(h=5,r=3,center=true);   
```
The code above would be a valid code that would make something in the OpenSCAD. The first three rows define x, y and z variables. Then there is translate command, that moves the position on the plane where the object will appear. Next command, nested in this translate command (to indicate that this happens after it), is rotate which rotates the object 90 degrees along the x-axis. The last command creates a cylinder with radius 3 and height 5.

{{< figure src="week4/openscad_test_cube.jpg" caption="**Figure 3:** First test program" height="450">}}

Using these simple commands I followed a [OpenSCAD tutorial](https://en.wikibooks.org/wiki/OpenSCAD_Tutorial/Chapter_1) and modelled a simple car.

{{< figure src="week4/openscad_car_final.jpg" caption="**Figure 4:** Simple car (tutorial)" height="450">}}

After doing this tutorial, I wanted a bit of challenge, so I decide to do my own model of train. First I did some brainstorming on how this train should look like:

{{< figure src="week4/brainstorm_train.jpg" caption="**Figure 5:** What am I making?" height="450">}}

And one with dimensions:

{{< figure src="week4/train_dimenstions.jpg" caption="**Figure 6:** Dimensions of the train model" height="450">}}

The commands I used to make the train were the same as above, plus two extra concepts:

**Combining objects** - you sometimes want more complex objects, and you can often achieve that using boolean operations - union, difference and intersection. 

```
union() {
    sphere(r=10);
    translate([12,0,0])
        sphere(r=10);
}
```

**Importing external models** - you can import models from the MCAD library, which is a library of components commonly used in mechanical designs. I used a round box model from it, which looks like and behaves similar to the cube, but has an option for rounding the edges.

```
use <MCAD/boxes.scad>
roundedBox(size=[15,5,30],radius=1,sidesonly=false);
```

{{< figure src="week4/openscad_final.jpg" caption="**Figure 7:** My train" height="450">}}

As seen on the figure above, I heavily parametrised the whole code, so all the train dimension could be easily changed (took a long time though).

One thing that I struggled to figure out was that when there is a nested translation (like translate, union, cube, translation, cube) then the second translation is not from [0, 0, 0] but from where the first translation "ended". It now makes sense when I'm thinking about it, but before I just somehow assumed that the translation is always from [0, 0, 0] just because that's how it is if there are not nested.

{{< figure src="week4/openscad_final_zoom.jpg" caption="**Figure 8:** My train zoomed in (so you can admire it from up close)" height="450">}}

Now the modelling part is done. I rendered, sliced and printed my train, and it turned out really nice. I was proud and happy to print something that I modelled.

{{< figure src="week4/train_cura.jpg" caption="**Figure 9:** My train in Cura" height="450">}}

The print looks nice, but there could be some adjustments. For example, I haven't realised that because the wheels are connected to the main body of the train with tiny rods, it was difficult to get them of the printer bed in one piece. (I broke one of the bigger wheels in half when I squeezed it to get the train from the printer). The other thing that could be improved is that you can see the infill between lower part of the train body and the upper part of the train body (Figure 9). Perhaps I haven't overlapped those two cubes enough in the OpenSCAD?

|    |    |    |
|----------|----------|----------|
|{{< figure src="week4/train_printed.jpg" caption="**Figure 10:** First self-designed project printed" height="450">}}| &nbsp; &nbsp; |{{< figure src="week4/train_mistake.jpg" caption="**Figure 11:** Still room for improvements" height="450">}}

Overall I would say that in some cases, this is the perfect software to use. When I want to make something where the same part is used multiple times (module), or where some steps are repeated (using loops), this would be my software of choice. However, the coding takes time. If I want to make a simple train as the one above, I would probably make it in the Fusion 360 as that would probably take less time. On the other side of spectrum, the software is also not robust (can't handle very complicated design made out of thousands of lines of code), and therefore maybe can't be used in industry.
## SolveSpace

SolveSpage is a software that was introduced during the lecture. I haven't used it before, but I wanted to try to learn it.

I really like how the "degrees of freedom" was visible during the sketch phase - it makes me want to have the little green "OK" there :)

Regarding my model: there was a plant at home that would benefit from a bigger pot, so I decided to make one - it seems simple enough, but also a bit more complicated than just some boxes, so I can get to try more features of SolveSpace.

{{< figure src="week4/pot_dimensions.jpg" caption="**Figure 12:** Sketch of a pot I want to make" height="450">}}

This is how the SolveSpace looks like when you open a new project: 
{{< figure src="week4/solvespace.jpg" caption="**Figure 13:** SolveSpace blank project" height="450">}}

At first (coming from a Fusion360 background) I made a really nice and detailed sketch where I wanted to extrude only some part of it. However, it turns out that this is not possible and probably also a bad practice in SolveSpace. I was a bit frustrated and didn't understand how it works, and was looking for a hacks how to extrude specific surface.

{{< figure src="week4/failed_design_solve_space.jpg" caption="**Figure 14:** First (failed) sketch" height="450">}}

However, what I needed was to change my perspective. SolveSpace is not Fusion 360, and it works differently. After I adjusted to it, and understood that I need to sketch what I want to be extruded (at that time, then sketch something again), the whole process started to make sense and I progressed quickly.

{{< figure src="week4/looks_messy_solvespace.jpg" caption="**Figure 15:** What I am doing" height="450">}}

I made the cylindrical shape by revolving the sketch around the z-axis. Then the new sketch plane was selected using a point on that plane and selected the "New group in new workplane" button.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week4/successful_design_solve_space.jpg" caption="**Figure 16:** Final product - the pot" height="450">}}| &nbsp; &nbsp; |{{< figure src="week4/successful_design_solve_space_2.jpg" caption="**Figure 17:** Bottom part of the pot" height="450">}}

The final product turned quite nicely. It took around 4.5 hours to print, but that's because the sides of the pot weer quite thick - four full rotation per one layer. If I were to print another pot, I would make it a bit thinner, which would then take less time to print.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week4/pot_printed.jpg" caption="**Figure 18:** Printed pot" height="450">}}| &nbsp; &nbsp; |{{< figure src="week4/pot_from_angle.jpg" caption="**Figure 19:** Printed pot from angle" height="450">}}
|{{< figure src="week4/final_pot_1.jpg" caption="**Figure 20:** Almost invisible" height="450">}}| &nbsp; &nbsp; |{{< figure src="week4/final_pot_2.jpg" caption="**Figure 21:** Surprise, it's inside!" height="450">}}

From my perspective, the advantage of SolveSpace is that it is perhaps less complicated than Fusion 360, and I found the interface more intuitive. The negative side is that SolveSpace is of course less powerful, and also I am still not totally sold on how the sketching and extruding works.