---
title: "Week 11: Output devices"

---
## Not final
I will still modify this page, as I haven't measure power consumption of a power device. Hope to do that soon.
## Introduction
The week we continued working with PCBs. Our task was to design and make a board that would include an output device.

## The plan
I would like to make a pig shaped board with a mini speaker and a button. When you press the button, the pig would oink.

**Optionally** it would be nice if the board would have a battery included. 

## The design
|    |    |    |
|----------|----------|----------|
|{{< figure src="week11/schematic.jpg" caption="**Figure 1:** The schematic of the desired circuit" height="450">}}| &nbsp; &nbsp; |{{< figure src="week11/pcb_design.jpg" caption="**Figure 2:** PSC board design" height="450">}}

I used the KiCad tool to make the design. I don't think the speaker I used was in the fab library (or at least I haven't found it), so I put some general speaker there. After that I cut it using the milling machine in the lab. 

{{< figure src="week11/cut_pig.jpg" caption="**Figure 3:** The board cut" height="450">}}
## The soldering

{{< figure src="week11/short_circuits.jpg" caption="**Figure 4:** Checking for the short circuits" height="450">}}

Next I looked for the components that were needed. I am using the female headers so the Xiao board can be attached and reattached smoothly. Decided to use the rivets so the header sticks there better, and it is connected to the copper.

{{< figure src="week11/materials.jpg" caption="**Figure 5:** All the components needed" height="450">}}

{{< figure src="week11/rivets_first.jpg" caption="**Figure 6:** First rivet was a fail (got connected to the main body). I guess next time there should be even bigger offset." height="450">}}

|    |    |    |
|----------|----------|----------|
|{{< figure src="week11/close.jpg" caption="**Figure 7:** Soldered female header" height="450">}}| &nbsp; &nbsp; |{{< figure src="week11/bottom.jpg" caption="**Figure 8:** Only necessary pins soldered" height="450">}}

The first try on soldering the switch was a big fail and recovering was long. My soldering skills are still quite poor, but at least I got better at fixing my mistakes. For the longest time there was a short circuit somewhere (that I couldn't find) but I managed to find it at least.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week11/switch.jpg" caption="**Figure 9:** Can someone tell me how to open this in a non-violent way?" height="450">}}| &nbsp; &nbsp; |{{< figure src="week11/holding_switch.jpg" caption="**Figure 10:** Tried to hold the switch with the octopus holder" height="450">}}

I really liked the "digital microscope". It made is easier to see.

{{< figure src="week11/close_up_solder.jpg" caption="**Figure 11:** Messed up, trying to find the mistake" height="450">}}

Then I manually drilled the holes for the speaker. I also wanted to use rivets, but the holes were too small, so I had to redo them (multiple times). I think I maybe put the rivers upside down? Not sure if that's possible, need to investigate.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week11/drilling.jpg" caption="**Figure 12:** Drilling holes for the mini speaker" height="450">}}| &nbsp; &nbsp; |{{< figure src="week11/warrior_pig.jpg" caption="**Figure 13:** Warrior pig (looks rough)" height="450">}}

At the end the pig was quite scratched :D (battle scars). I messed us soldering the speaker as well, but I'll get to that later.

## The coding


{{< figure src="week11/code.png" caption="**Figure 14:** Using pitch.h that defines notes as numbers" height="450">}}

## (not so) Final pig

{{< video src="week11/final_pig.mp4" width="400" caption="**Figure 15**">}}

## Inprovisation

{{< figure src="week11/secret.jpg" caption="**Figure 16:** The secret fix" height="450">}}

## Todo:
- measure the power consumption of the output device
- include source files in documentation
