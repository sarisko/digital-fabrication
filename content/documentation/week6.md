---
title: "\nWeek 6: Embedded Programming"

---

This week we worked on embedded programming. We were given a Seeed XIAO RP2040 board from Seeed studios.
Overview: https://wiki.seeedstudio.com/XIAO-RP2040/

{{< figure src="week6/board_overview.jpeg" caption="**Figure 1:** Hardware overview" height="450">}}


### Soldering
The useful tutorial to soldering headers [here](https://mycourses.aalto.fi/mod/resource/view.php?id=1015021&class=d-flex%20align-items-center). 

I did solder some stuff before, but I am still need more practising. Should've watched more tutorials. I like that there are quite a few tools in the lab (such as flux pen and desoldering braid), was happy to get to try all of them. 

|    |    |    |
|----------|----------|----------|
|{{< figure src="week6/workbench_setup.jpg" caption="**Figure 2:** My workbench setup" height="450">}}| &nbsp; &nbsp; |{{< figure src="week6/flux_pen.jpg" caption="**Figure 3:** The Flux pen" height="450">}}

Before I started soldering, I turned on the fan. After a bit, I had a first attempt at soldering the headers, but it turned out not ideal.

{{< figure src="week6/messed_up_solder.jpg" caption="**Figure 4:** Messed up solder where two of the rightmost pins are connected together" height="450">}}

Therefore, (with Yu-Hans help) I used the disordering braid to remove the excessive tin.

{{< figure src="week6/desoldering_braid.jpg" caption="**Figure 5:** Removing the extra solder" height="450">}}

After this I managed to solder the pins (although not beautifully). I now understand that the tin is supposed to cover the "golden" area where the pins are, so they are securely in place. To check that there is no short circuit, I used the voltmeter (the resistance setup) to check all the neighboring pins.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week6/resistance_check.jpg" caption="**Figure 6:** Using voltmeter to check for short circuit" height="450">}}| &nbsp; &nbsp; |{{< figure src="week6/done_soldering.jpg" caption="**Figure 7:** Final look on my board after headers are attached" height="450">}}

## Programming
I wanted to use Arduino to program the board, however for some reason on my computer I couldn't find the Raspberry Pi Pico/RP2040 - I will solve this problem later, but for now let's try using the MicroPython and the [Thonny IDE](https://thonny.org/).thonny

Installing Thonny:
```
brew install --cask thonny
```

I haven't used MicroPython with a board yet, so it's a nice learning experience. I am not sure if I would use it in production as it is still Python - too big for some microcontrollers, somewhat clumsy and not real time. But its nice for testing, and I do believe it is easier to program. Documentation can be found [here](https://docs.micropython.org/en/latest/reference/index.html).

The initial setup was a bit tricky, and can be found [here](https://wiki.seeedstudio.com/XIAO-RP2040-with-MicroPython/). Interesting steps were that the board should be booting when connecting to the computer, and then how MicroPython is installed to the board.

### Fist program
To showcase that the board works, and it's correctly set up, I tried to run one of the default programs (something like blink in the Arduino library).

{{< figure src="week6/thonny_ide.png" caption="**Figure 8:** The first blink program" height="600">}}
{{< figure src="week6/board_blink.jpg" caption="**Figure 9:** Board blink" height="450">}}

### More sophisticated program
I made a program that the colour changes when you press the button.

{{< figure src="week6/button_ide.png" caption="**Figure 10:** MicroPython code for blink when button is pressed" height="600">}}
{{< figure src="week6/button_board.jpg" caption="**Figure 11:** The colour changes when the button is pressed" height="450">}}

{{< video src="week6/button_blink.mp4"  >}}

### Serial connection
As mentioned, I had problems with Arduino. It turns out it was because I forgot to add Additional Boards Manager URLs in Preferences (as shown in this [tutorial](https://wiki.seeedstudio.com/XIAO-RP2040-with-Arduino)).
Now, using Arduino interface, I made a simple command like code where the commands I was writing on the computer where changing the LED color on the XIAO board to some pre-defined colors.
{{< figure src="week6/code_arduino.png" caption="**Figure 12:** Arduino IDE, serial connection with eh board" height="600">}}
Video of the code execution:
{{< video src="week6/serial_connection.mp4"  >}}
### What has been done outside this week tasks:
- nicer figures (side by side, caption, resizing)
- a lot more week 2 documentation
- added documentation about image compression (week3)