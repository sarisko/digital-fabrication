---
title: "\nWeek 1: Web basics"

---

In the first week, we talked about the course structure and the basics of Git and Git CI. We also developed and deployed the first basic version of a website that will document our progress throughout the course.