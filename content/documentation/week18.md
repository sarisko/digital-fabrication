---
title: "Week 18: Applications and Implications"
---
## Introduction

This week I continued to work on my final project. On this page will further describe the circumstances of my project, and answer some specific questions.  
  
Some questions are answered in the Final project documentations, which can be found [here](https://sarisko.gitlab.io/digital-fabrication/final_project/).

## What will my project do?

My final project - a self-balancing robot - is a two wheeled robot that can maintain balance and stay in an upright position. It is using a MPG6050 accelerometer that feeds data to the PID algorithm that determines where and with how big of a force should the robot move to stay balanced.

## What has been done beforehand?
Making a self-balancing robot is not a new thing and many people did it in different variations. I choose this project because I wanted to learn new things such as coding for embedded devices, PCB milling or designing the cover, and also because I was interested in the "moving" aspect of the robot. I researched what other people did before starting this project, and it influenced my work a lot (as it is described in my final project page).  
  
The most important one is the [DIY Self Balancing Robot](https://circuitdigest.com/microcontroller-projects/arduino-based-self-balancing-robot) by Aswinth Raj. His explanation and design were the most valuable, and I am trying to build upon his work.

There were some other robots on Instructables that I read about, namely [The breadboard Self-Balancing Robot](https://www.instructables.com/The-Breadboarded-Self-Balancing-Robot/), [2-Wheeled-Self-Balancing-Robot](https://www.instructables.com/2-Wheeled-Self-Balancing-Robot/) and [Arduino Self-Balancing Robot](https://www.instructables.com/Arduino-Self-Balancing-Robot-1/).

The next step from those robots is the [Codesys Powered Self Balancing Robot](https://www.youtube.com/watch?v=EwrQEsFmL4E) which in addition to balancing on two wheels is also capable of moving based on the signal from remote controller. In my design I am also trying to add an option for this feature with The Xiao ESP32 attached to the Arduino. 

Those are the amateurs self-balancing robots. In the industry the state of the art would be the robots by [Boston Dynamics](https://www.bostondynamics.com/), or as I have recently seen a [Mini Cheetah](https://robotsguide.com/robots/minicheetah) which is also open source (link to the theses [here](https://dspace.mit.edu/handle/1721.1/118671)).

## Materials and components will

Bellow is the list of all materials I used for the project. I always used the unit price simulating how much would it cost if I purchased the component alone (not in bulk). However, there were some components that you have buy as a whole even if you use only a fraction for this project - solder and rivets. What I did was I checked how much these components cost, and them I only added a part of this cost to my Bill of material (the thinking behind it is that even if I buy it for this project, I can use the rest of it for some future project).

<iframe src="https://docs.google.com/spreadsheets/d/17TLfAV2IrNe_drBtCOaLK6oHDnumnsX1nCMZwEV2V98/edit?usp=sharing" width="100%" height="750" frameborder="0" style="border:0"></iframe>

If I were to reproduce this project, e.g. build another robot, I would either buy the parts from the local FabLab, or purchase them of various websites. Here are some of my favorite ones:
- For the bulk shopping I believe the [DigiKey](https://www.digikey.fi/en) is the best and also has the largest selection. It is also quite internationl, meaning if someone abroad wants to reproduce your project, they can probably find DigiKey in they country, or the DigiKey has a shipping to their country 
- For some specific microcontrollers I like to use [Aliexpress](https://www.aliexpress.com/). It usually takes a long time till the components arrive, but it is very cheap and in my experience the components were a good quality
- The is also a Slovak hardware store that I particularly like, [GM Electronics](https://www.gme.sk/), mostly because I find it a bit cheaper than DigiKey, and you can pick up the components the next day (which is superfast, and also you don't have to pay for shipping). Slovakia only though!
- Some other honorable mentions are [Amazon](https://www.amazon.de/), [Verkkokauppa](https://www.verkkokauppa.com/), [K Rauta](https://www.k-rauta.fi/) for some things, and [Alza](https://www.alza.sk/)


## Summary of the project, reflection and evaluation

At the beginning of the course I was not decided on what final project I want to make, and it did reflect on the result. At first, I wanted to make some smart furniture, but I wasn't that hyped about it. Then I wanted to make interactive maps (something almost like a learning tool), but I didn't have a good design by the time we had to come up with the final project.  
  
At last, I choose to build a robot. As it seemed a bit difficult of a task for a beginner, I divided the project into three phases -> making a moving robot (small car), self-balancing robot and some robot with interesting actuators. 

I got stuck for a long time on a moving robot, unable to make the brushless motors move the way I wanted them to move. So at some point I skipped this stage, and moved on to making a self-balancing robot. Sure I had some troubles with coding part, but in the end I made a first version that was able to balance on two wheels. However then the deadlines came, and so my progress was limited. I made around 3 PCB boards which all failed for one reason or another. I am however still determined to make one proper one, do soldering right, and have a battery powered robot.

 But at a current stage, my project is still a work in progress. The robot is connected to a power source and using the L298N H-bridge. It is somehow a proof of concept, as the balancing feature is there, but I want to be proud of it, so I will make it better!

 I would also like to mention that I enjoy making this project. It was strange as I was very inexperienced, and I would do many things differently now (which is probably good! It means that I learned a lot). I feel like I have a learning curve where I like to explore a new topic in more detail, which (maybe) makes my progress slower. I also get distracted by the side projects that I find exciting.

 ## Questions to answer
Can I make the self-balancing robot using only the Xiao PR2040 module (no Arduino)?  


