---
title: "Week 13: Input devices"

---

This week we are looking info reading sensor data, for example reading distance sensor, light sensor, thermistor... I chose to work with and accelerometer/gyroscope because it will be useful for my final project.

## Arduino code

[This website](https://learn.adafruit.com/adafruit-lsm6ds3tr-c-6-dof-accel-gyro-imu) was my starting point when working with Adafruit accelerometer and gyroscope. It contains the pinouts, library installation guide and a very helpful example code.

The code I used was a modified version of a code mentioned above, here it can be viewed --> [[.ino file]](/digital-fabrication/files/input_device.ino)

## The test

I was not really familiar with the acc sensor ([Adafruit LSM6DS3TR-C 6-DoF Accel + Gyro](https://learn.adafruit.com/adafruit-lsm6ds3tr-c-6-dof-accel-gyro-imu/overview)), so I first tried to work with the sensor on a breadboard to see if I can read it and control the LEDs.

{{< youtube 8vGV7opigE8 >}}

So the proof of concept works. Now I'll make a custom PCB board for it.

## The board design
|    |    |    |
|----------|----------|----------|
|{{< figure src="week13/schematic.png" caption="**Figure 1:** The schematic design of the new board" height="450">}}| &nbsp; &nbsp; |{{< figure src="week13/pcb.png" caption="**Figure 2:** The PCB board design" height="450">}}

Download the KiCAD files - [[.zip]](/digital-fabrication/files/input_device.zip)

At first, I made a bad board - I was checking the documentation of the XIAO board, and was trying to connect the SDA and SCL with the accelerometer, so they can communicate. Unfortunately, when connecting the pins in the schematic editor, I connected the pins 4 and 5, not the digital pins D4 and D5 (as I was supposed to). 
{{< figure src="week13/missaligned.png" caption="**Figure 3:** The digital pins are numbered from 0 while normal pins are numbered from one" height="450">}}
I made this board, soldered it, tested the components... and then found this mistake! It was a learning experience but also quite a waste of resources. At least I got better with the PCB milling machine in the process. The conclusion is that I need to be extra careful with pins then doing the wiring in KiCAD schematic design.

## The final board

I wanted to make the LED's light up at a certain angle, and then turn off below that angle. Honestly I was surprised how well it works, the precision was impressive. Can't wait to work with it some more!

{{< video src="week13/board_led.mp4"  >}}

