---
title: "Week 19: Invention, IP and Income"

---

## Future plans
The final project was an opportunity for me to explore how robots move, and try to build my own. It was mostly learning tool, and it doesn't do much other than moves around (one can even call it a toy).

To polish my self-balancing robot, I would like to ***make a better case***, to hide the cables and make it look more sleek and professional. The second thing I am planning to do is to ***explore the Wi-Fi capabilities of the Xiao ESP32*** and make the robot move based on external control (while balancing). Finally, I am also interested in designing and ***making my own wheels*** as it is one of the more expensive items in the Bill of Materials.

In the future, I would like to build a different, more advanced robots. Maybe try different approaches in how the robot moves around (so instead of using wheel make more humanoid robot like [this](https://www.youtube.com/watch?v=KSvLcr5HtNc) or something completely different like [this](https://www.youtube.com/watch?v=mZJ6yAndfCo)), or perhaps add a grabbing function.

I am still fascinated by the [Boston Dynamics](https://www.bostondynamics.com/) robots (which were the main influence why I did self-balancing robot as my final project), and their [balancing capabilities](https://www.youtube.com/watch?v=tF4DML7FIWk), and in this project I was trying to learn more about it, understand how they do it, and also build the robot itself for my portfolio.

## Licensing

So what is licensing? I asked ChatGPT:
> Licensing refers to the legal granting of permission by the owner of intellectual property rights to another party to use, distribute, or modify that intellectual property. It allows individuals or organizations to use someone else's intellectual property legally while respecting the owner's rights.

Some related terms are Intellectual Property (IP, which can include copyrights, patents or some other secrets, and they are granted to those who created the original work), License Agreement (legally binding contract between licensor and licensee), Royalties and Fees (licensee may pay royalties or license fees to the licensor). 

Open source license is special types of license which usually promotes collaboration, transparency, and the sharing of source code without financial gain. (E.g. you publish your code/software online and anyone can see it and use it, sometimes even modify it and propagate further)

Open source licenses can be broadly categorized into two main categories:
1. **Copyleft Licenses:** require that any derivative works or modifications made to the original software must also be licensed under the same terms. This ensures that the subsequent versions of the software remain open source and freely available. (example: GNU General Public License (GPL))
2. **Permissive Licenses:** have fewer restrictions on the use, modification, and distribution of the software. They allow more flexibility for developers and users, often permitting the software to be included in proprietary or commercial projects (example: MIT License)

### Examples of Open source licenses:

**MIT License:** permissive open source license that allows software to be used, modified, and distributed, including for commercial purposes, with minimal restrictions. It is widely adopted and considered business-friendly.

**Creative Commons (CC) Licenses:** not specifically designed for software, but it provides a framework for licensing creative works, such as text, images, and music. They offer a range of permissions and restrictions that can be customized to meet the creator's intentions (website bellow)

**GNU General Public License (GPL):** copyleft license that ensures software remains free and open source. It requires derivative works to be licensed under the same terms and provides strong protections for users' freedoms.

If you are not sure what to do, [this Creative Commons website](https://creativecommons.org/choose/) can help you choose the right license for your project.

I decided to use an **Attribution-NonCommercial-ShareAlike 4.0 International license** on my project and added it to my website and git. It means that adaptations of my work can be shared as long as the others share alike, and commercial use of my work is not permitted.

I chose this license because I would like my work to be publicly available (open-source), but I don't want other people to use it without acknowledging me. The share alike license is perfect, because if other people will use and modify my work, they have to share it similarly than I did - so for example it cannot happen that I share some of my work, someone else will use part of it, but release it under ***different*** license that would for example allow the commercial use of it, and in the end my product will be used commercially without me knowing it.

I think that the Open Source community is quite nice, and I use the work of other people often. Therefore, I would like to give back, e.g. contribute with my work to the Open Source world, and hope other people will find it useful.

**Disclaimer:** Parts of the text were made / formulated by ChatGPT.

