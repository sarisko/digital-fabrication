---
title: "\nWeek 5: Computer-Controlled Cutting"

---
## Laser Cutting

This week we started to learn about laser cutting. We were introduced to the laser cutter in the lab, and our task was to try to play with the settings and cut something as a project.

## Preparations:
### Getting the laser cutter ready
- Safety first - ventilation must be on (maxed), and we need to make sure that the fire blanket and fire extinguisher are present. We need to watch the laser cutter at all times that it is on (there is also a small device that will ask you once in a while whether you are still present).
{{< figure src="week5/security_being_present.jpg" caption="**Figure 1:** Device that beeps to check if you are present" height="450">}}
- Place the material in the laser cutter. The laser cutter can cut different materials -> we did the test cuts with cardboard as it is the cheapest one, then we tried the wood of different thickness
- It is important to calibrate the distance between the laser cutter and the material so that we get the best focal point. There is a metal thing attached to the laser with a magnet that can be freed, and it should be just touching the material below, so there is a bit of friction. If there's not, we should adjust the laser cutter setting to either lift or push it down. NOTE: some laser cutters can do this automatically (even the one we have in lab, it was just broken for a bit) - when you are importing the material settings, there is an "Auto Focus" option there, which is "Off" by default, but we can set it to "Plunger"


### Getting the design ready
- Prepare the design we want to print -> this should be a 2D design that will be cut out. Note that the product will be 3D, but the third dimension is just the thickness of the material we are cutting. The design should be in an appropriate (vector) format such as .dxf, .svg, .eps or .pdf. 
- Open your design in Adobe Illustrator. Make sure all the lines you want to cut are 0.01mm thick.
- After we are satisfied with the design, we can try to “print” it (control P), which will open the print dialogue. There is a special add-on for the laser cutter, as seen on image bellow. Change the "Media Size" to Custom.
{{< figure src="week5/print_dialogie_adobe.jpg" caption="**Figure 2:** Adobe Ilustrator print" height="450">}}
- After selecting print, you should see the live image of what's happening inside the laser cutter in a new program (at this point all the steps of “Getting the laser cutter ready” should be completed, and the laser cutter turned on) -> as seen on image bellow. If the camera doesn't work, restart the laser cutter.
{{< figure src="week5/LC_program.jpg" caption="**Figure 3:** Laser cutter program" height="450">}}
- Place the piece where you want it on the image. Make sure it's not too close to other edges (it can ruin the cut).
- Make sure that there is a vector and the engraving for the correct things.
{{< figure src="week5/vector_and_engrave.jpg" caption="**Figure 4:** Vector and Engraving" height="450">}}
- Select the right settings for the material you are using (there are pre-defined settings for different materials), adjust them if needed.
{{< figure src="week5/laser_cutter_setings.jpg" caption="**Figure 5:** Different setting" height="450">}}
- You can select "Start cutting" on the computer. Then select the correct file name on the laser cutter display, and press play. The cutting should start now.
{{< figure src="week5/laser_cutter.jpg" caption="**Figure 6:** Press Play" height="450">}}
### Practical tips:
- The laser cutter is connected to a computer next to it. It is nice to have the design prepared on a USB drive. The other option is to log in using the Aalto account, but that might take a bit of time
- The strength of the engraving does not change the colour of the engraving, just the depth of the cut
{{< figure src="week5/engraving_diff.jpg" caption="**Figure 7:** Engraving comparism" height="450">}}

- Always open the laser cutter with both hands
{{< figure src="week5/opening_laser_cutter.jpg" caption="**Figure 8:** Laser cutter opening" height="450">}}

## Kerf - What is it?
Kerf is the width of the material removed during cutting, which is determined by the laser beam's diameter and the material being cut. Measuring kerf when laser cutting is important for achieving accurate cuts. 
How to calculate the kerf: 
- Set up the laser cutter and ensure it is properly calibrated for the material you're cutting
- Cut a test piece of the material you plan to use 
- Measure the width of the test piece 
- Calculate the kerf -> subtract the width of the cut piece from the width of the original material. Divide the result by two to get the kerf. For example, if the original material is 3 mm wide and the cut piece is 2.8 mm wide, the kerf would be (3 mm - 2.8 mm) / 2 = 0.1 mm
- Use the calculated kerf measurement to adjust your design

It's important to note that kerf can vary based on the type of material being cut, the laser's power, and other factors. It's recommended to test the kerf for each new material or laser setting to ensure accurate cuts.

Link to the group repository: https://aaltofablab.gitlab.io/laser-cutting-2023-group-a/kerf.html

## Documentation of the actual cutting

### Cardboard squares
Our first cut was way out of focus -> the line was quite thick, and it was not possible to easily remove the squares from the cardboard plate. This was mainly because the material was bent, next time we can attach it using tape.
{{< figure src="week5/bad_cut.jpg" caption="**Figure 9:** Bad cut" height="450">}}

After we fixed that, the cut was way cleaner, so we tried to calculate the kerf. The image below is the second cut, three squares 30 mm x 30 mm without any adjustments. Together the length of these three squares was 89.4 mm. 
{{< figure src="week5/kerf_measure_one.jpg" caption="**Figure 10:** Three squares without kerf" height="450">}}

It seems like there was 0.6 mm cut away from the squares, 0.2 mm from each. So we made the squares 0.2 mm bigger, cut and measured it again. This time, the length was precisely what we would expect.
{{< figure src="week5/kerf_better.jpg" caption="**Figure 11:** Three squares with kerf" height="450">}}

### Bird house
I was very keen on trying to cut something real, something useful (moderately fast because I was running out of time). So I decided to make a bird house (I do have a very small backyard where I can put it). Desperate times calls for desperate solutions, so I decided to use one of the online tools to help me create the box which I can later modify. I used https://makeabox.io/. I chose this one because it also counts in the kerf.
|    |    |    |
|----------|----------|----------|
|{{< figure src="week5/sketch.png" caption="**Figure 12:** Sketch of the project" height="450">}}| &nbsp; &nbsp; |{{< figure src="week5/makebox.png" caption="**Figure 13:** Makebox screenshot" height="450">}}

This was the first time cutting with wood - it was extra thrilling. I think the setting were just right as everything was cut nicely.
|    |    |    |
|----------|----------|----------|
|{{< figure src="week5/bird_thing_cut.jpg" caption="**Figure 14:** Laser cutting the bird house" height="450">}}| &nbsp; &nbsp; |{{< figure src="week5/my_first_cut.jpg" caption="**Figure 15:** The cut didn't work out the way we wanted it :') " height="450">}}

It was honestly a bit of a failure - the pieces did not hold together well, they were too loose. Later I found out that the wood we used was only 3 mm thick, not 4 mm (which I counted with in my calculations). However, at least it was somehow easily fixable, and it gave me the opportunity to use the wooden workshop. Solomon showed me around, and I used wood glue to make sure the bird house stays together.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week5/glue.jpg" caption="**Figure 16:** Excited Sara" height="450">}}| &nbsp; &nbsp; |{{< figure src="week5/bird_thing_done.jpg" caption="**Figure 17:** Final product" height="450">}}

At the end I attached some extra wood on the bottom, so the birds can rest there. I consulted by friend who is a bird ringer, and she advised to leave one side unglued (so it can be removed) because the inside of the bird box needs to be cleaned after each nesting season. She also confirmed that the wood glue won't be toxic to birds. I would like to further improve the bird box, perhaps stick something nice to the side.

## Mini shelf
During the CNC week, I would like to make a shelf for my "already used clothes" (clothes not dirty enough to go the laundry, but not clean so they can't go back to the wardrobe). I made a design, but I am a bit unsure about the stability of it (and how it would look like), so I will make a smaller model (the size is 20% of the original model) using the laser cutter.

### The design

{{< figure src="week5/the_design.jpg" caption="**Figure 18:** The design of the shelf" height="450">}}

The most interesting/complicated part are the joins shown in Figure 19 that are supposed to hold the upper part of the shelf. The idea is that the lower desk is going to "slide through" the leg, and then the asymmetric pieces will be added to it that would hold the upper shelf in place. I wanted it to look nice, like a part of design. You can also notice the right angles between the leg and stoppers for better support.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week5/leg.jpg" caption="**Figure 19:** The design of the stopper" height="450">}}| &nbsp; &nbsp; |{{< figure src="week5/sketch_to_print.jpg" caption="**Figure 20:** Sketch to be cut" height="450">}}

### Test cut

First I tried to cut just the joins (with small part of the leg to save space). The test pieces were so small that after the cutting I had to tape them to the plywood otherwise they would fall through the metal grid of the laser cutter and be lost forever.

{{< figure src="week5/tape.jpg" caption="**Figure 21:** First cut, very small pieces" height="450">}}

### Assembling

The video bellow shows the assembling of the shelf after cutting all the components. Since this was the first try, I didn't get the kerf right and so the shelf was very wobbly.

{{< video src="week5/timelaps_trimmed.mp4" width="400">}}

Some close up pictures of the joins/stoppers. After I managed to improve the fit, they worked fine and looked nice.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week5/legs_assembled.jpg" caption="**Figure 22:** Shelf from the side" height="450">}}| &nbsp; &nbsp; |{{< figure src="week5/shelf_side.jpg" caption="**Figure 23:** Looks like the stoppers work" height="450">}}

### Adjusting the fit
After cutting the test stoppers, I actually also cut three sample squares to calculate the kerf. It turned out to be 0.1 mm. Normally I would modify the parametrised design in Fusion 360,but Fiia showed me a convenient trick of how to make the design bigger/smaller everywhere at once (to include the kerf). In Adobe Illustrator first select the object you want to scale (make bigger/smaller), then **Object->Path->Offset Path**. You will be asked how much offset you want to create, and it will make a new line for you. It doesn't work well with sharp corners, but I didn't mind, and you can't spot the difference in the finished product. Also, one should not forget to delete the original lines, otherwise the laser cutter will cut them as well.

{{< figure src="week5/adobe_path.jpg" caption="**Figure 24:** Adjusting the 2D design to count in the kerf" height="450">}}

Then it was time for the second cut! The real speed of the laser cutter we have at lab:
{{< youtube 56O9sYJy9pw >}}

{{< figure src="week5/cut.jpg" caption="**Figure 25:** Second cut" height="450">}}

In the second iteration, I wanted to minimize wobbling of the shelf by making the leg/horizontal desk connection tighter. On the Figure 26 bellow, you can see both the first and second "generation". The top one is the first one, and one can notice that because the hole is too big, the gravity makes the shelf "lean down" at a smaller angle. The leg-shelf fit is better in second iteration, so we can see almost a right angle between them.

There was also a third (the final) generation, where the fit was even tighter.

{{< figure src="week5/comparism_fitness.jpg" caption="**Figure 26:** The difference between loose and tight fit" height="450">}}

### Finished product
Happy with it. The design can definitely be improved (it't now not wobbly, but perhaps a bit askew), but I tried to mainly focused on getting comfortable working with the laser cutter.
{{< figure src="week5/final_mini_shelf.jpg" caption="**Figure 27:** The final product. Looks cute." height="450">}}

### Final notes
Kerf explanation influenced by Chat GPT.

Met Jeti at the lab - hopefully I made a good first impression even though I didn't give him any treats.

{{< figure src="week5/doggo.jpg" caption="**Figure 28:** Jeti and I <3" height="450">}}
