---
title: "\nWeek 7: 3D Scanning and Printing"

---
 This week we worked with 3D printers. We focus on learning what a 3D printer can do and its limitations. On this page I will summarise what I know and my discoveries.

### Backgroung
I was first introduced to 3D printing (and to a concept of a rep rap printers) in 2020, with an old Prusa printer.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week7/prusa1.jpg" caption="**Figure 1:** Old Prusa printer front" height="450">}}| &nbsp; &nbsp; |{{< figure src="week7/prusa2.jpg" caption="**Figure 2:** Old prusa printer top" height="450">}}

When I got it, it was broken, so I tried to make it print properly again. It took a lot of time and efforts bit it was too much for a novice engineer. So I abandoned the project, but looking at it now, I believe it taught me how printers work (and don't work).

My second printer was (and still is) a Ender 3v2 which I got while working on a Syringe Pump project (https://gitlab.com/sarisko/syringe-pump ). 

{{< figure src="week7/ender.jpg" caption="**Figure 3:** Ender 3v2, we have a love/hate relationship <3" height="450">}}

It is one of the cheaper printers, but can print incredibly when there is a good care taken of it. I had to disassemble it when I was moving to Finland, and it still works fine. It however has two problems and that is that one needs to be pretty skilled when calibrating it (if it's not used for some time, it always needs recalibrating and cleaning) and the other problem is cheap extruder which should be changed -> mine broke and in the fall of 2022, and I haven't noticed for a long time, so I was producing only very bad prints (Figure 4) and I didn't know why.  

{{< figure src="week7/broken_prints.jpg" caption="**Figure 4:** Debugging the printer" height="450">}}

I tried many things such as changing the nozzle, or using an adhesive spray on the bed, hut ultimately I noticed that it is a problem with extrusion, and later I found a crack in the plastic extruder, which later completely broke when I was trying to change it (Figure 5). I bought the new extruder from 3D Jake (https://www.3djake.com/3djake ).

|    |    |    |
|----------|----------|----------|
|{{< figure src="week7/broken_extruder.jpg" caption="**Figure 5:** Broken plastic extruder" height="450">}}| &nbsp; &nbsp; |{{< figure src="week7/new_extruder.jpg" caption="**Figure 6:** New metal extruder" height="450">}}

## Common problems
After printing for quite some time, I would say the common problems with printers (that I worked with) are:
- Bad (old) filament - when the filament is exposed to air for too long, it can absorb water, and then it breaks easily. This can be prevented if the filament is stored properly in a sealed bag
- Print not sticking to bed - most of the time this is caused by bad settings/calibration, e.g. the nozzle is too far from the bed during the first layer. Sometimes this can be caused by the old/broken bed that is too scratched. It can be sometimes solved just by moving the print to a different part of bed, or heating the bed to a higher temperature for a first few layers
- Extrusion - sometimes bad prints can be caused by the "extruding wheel" being blocked with filament, so there is not enough friction to push the extruder properly. Or the Cheap Ender printer often have low quality extruders, and they can break easily.
## Design rule test
The second task was to try one of the models in the design test rules, and print it with FDM printer. I printed it with my Ender 3v2, the settings were:
- first two layers 50% speed, after that 100%
- first two layers bed heat 65C, after that 50C
- nozzle temperature 200C

The prints turned out really nice, I am quite proud of how well I calibrated the printer. It managed to print it without any major issues. The only thing you can notice were a very very thin "hairs" of filament between the bridge, but those can be easily removed.
|    |    |    |
|----------|----------|----------|
|{{< figure src="week7/unsupported_test.jpg" caption="**Figure 7:** Bridges" height="450">}}| &nbsp; &nbsp; |{{< figure src="week7/wall_thickness_test.jpg" caption="**Figure 8:** Wall thickness" height="450">}}

{{< figure src="week7/wall_test_before_stl.jpg" caption="**Figure 9:** Design test rules: Wall thickness in Cura before slicing" height="450">}}

{{< figure src="week7/wall_test_after_stl.jpg" caption="**Figure 10:** Design test rules: Wall thickness in Cura after slicing" height="450">}}

Reflection: --todo ?

## Unique object
### Iteration 1
Our final task was to design an object that cannot be made using subtractive manufacturing, such as laser cutting or CNC machime. I designed a very simple hollow cute, and printed it. It turned out really nice :)
|    |    |    |
|----------|----------|----------|
|{{< figure src="week7/fusion_cute.jpg" caption="**Figure 11:** Hollow cube modelled in Fusion 360" height="450">}}| &nbsp; &nbsp; |{{< figure src="week7/cube_sliced.jpg" caption="**Figure 12:** Sliced hollow cube" height="450">}}

{{< figure src="week7/cube.jpg" caption="**Figure 13:** Final print of the cube" height="450">}}
### Iteration 2
As per the feedback I received during the lecture, I modeled an extra sphere into the hollow cube, so it is "locked" inside, but free to move. I am quite rusty with Fusion 360, so it was a bit tricky, but with some help I managed to make it work.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week7/figma_cube_sphere.jpg" caption="**Figure 14:** Hollow cube with sphere inside modelled in Fusion 360" height="450">}}| &nbsp; &nbsp; |{{< figure src="week7/cura_cube_sphere.jpg" caption="**Figure 15:** Sliced hollow cube with sphere inside" height="450">}}

Fun fact is that the sphere and the hollow cube were added separately to the slicer, since I modelled the sphere to be exactly in the middle of the hollow cube, so in the Fusion it is floating a bit. If I added them together, the sphere would be floating, so I would need to add supports. But then these supports would also support the top of the cube which I don't want. However, when you add these two objects to the Cura separately, is makes the bottom part touch the bed, so in my case it moved the sphere down sop its nicely on the ground inside the cube.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week7/broken_cs.jpg" caption="**Figure 16:** First broken print" height="450">}}| &nbsp; &nbsp; |{{< figure src="week7/fixed_cs.jpg" caption="**Figure 17:** Second successful print" height="450">}}

The first print looked a bit bad for no reason -> probably just the first one/two layers didn't stick to the bed properly, so the cube and sphere were a bit degenerated. What I usually do in these situations is to just try again, but this time I also moved a print in the slicer so it is not in the middle of the bed. Since the middle is the most used place, the filament may not stick there that well, and I also had some accidents there in the past where the nozzle was too close to the bed and scratched the surface there. But after moving and restarting the printing, the final print came out really nice.

{{< figure src="week7/cs.jpg" caption="**Figure 18:** Now I have a nice toy :)" height="450">}}

## SLA printers
- Todo: document here the introduction session about the SLA printers that I attended

