---
title: "Week 17: Wildcard week - Glass Casting"

---

## Introduction
In this week, we had an option to explore a digital fabrication method beyond the capability of a basic FabLab.  
The option we had were for example waterjet cutting, vacuum foaming or injection molding.  
  
I chose to do **glass casting**, because I am interested about the material, and I want to learn how is it done. There is not really anything I need to make from glass for my project, so I tried to make this a bit "artsy".

## Limitations
The model should fit into 10 cm x 10 cm x 10 cm square. The features shouldn't be smaller than 6 mm.
## Design
During a spring Tokyo sale I bought a little glass object:

{{< figure src="week17/glass.jpg" caption="**Figure 1:** Glass sculpture I have at home" height="450">}}

I really liked it! Apparently its shape can help you relieve anxiety when you play with it in your hands. The person who was selling it had quite a few different colors, which is also something I would like to experiment with.

So I decided to make my own little "teeth". The original plan was to make them out of clay, and then 3D scan them, but due to the lack of time I just had to make the 3D model in a CAD software. Honestly, it's not the best. So far I only used CAD software such as Fusion360 for making precise models, where the dimensions mattered, and usually they were also easier to describe. I think the design I made is not too bad (I learned something about fusion once again while doing it), but next time I would rather 3D scan it.

{{< figure src="week17/teeth_model.png" caption="**Figure 2:** My model of glass teeth" height="450">}}

In the glass casting process, we insert glass shards into the mold, and then melt the glass. However, there is some air between the shards, so we somehow need to insert more of it so then when it melts is has the same volume as our model.
  
For this reason, we attach a cone to our model, where we will add the glass shards. This cone should have the same, perhaps a bit bigger volume than the model.
  
Fusion had a nice feature where it can tell you the volume of the Body (right click on body -> Properties). It will also show you a bounding box for the model, which is also useful as I can't exceed 10 cm in any direction. Now I know the volume I need, and also the maximal height of the cone (10 cm - the height of the model). I found a [nice website](https://www.omnicalculator.com/math/cone-volume) where you can play with the parameters of the cone.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week17/bounding_box.png" caption="**Figure 3:** Fusion360 Body properties showing the bounding box of the model" height="450">}}| &nbsp; &nbsp; |{{< figure src="week17/cone_calc.png" caption="**Figure 4:** Calculating the right parameters for the cone" height="450">}}

After I got the parameters, I also made a model of the cone. I will later attach them together (after the print, so I don't have to print the supports).

{{< figure src="week17/cone_model.png" caption="**Figure 5:** Cone model Fusion360" height="450">}}

## Printing
This models had to be printed with the Natural PLA (Transparent) so that when they are melted later in the process, no dye (or other things that can be added to a non-natural PLA) will be left in the mold.
  
{{< figure src="week17/gif.gif" caption="**Figure 6:** Printing process" height="450">}}

Tried to use small infill (10%) so the printing will be faster (less waste of material), and the melting will go well (hopefully). As you can see, the features of the model are smaller than recommended 6 mm, which is a bit of risk, but if you don't live on the edge, you are taking up too much space.

|    |    |    |
|----------|----------|----------|
|{{< figure src="week17/printed.jpg" caption="**Figure 7:** Teeth and a cone printed with Natural PLA" height="450">}}| &nbsp; &nbsp; |{{< figure src="week17/assembled.jpg" caption="**Figure 8:** Models attached together" height="450">}}


## Future work - August?
(The glass workshop is now closed, so the rest will be carried out in August. I will however describe the process (or at least what I think will happen).)  
  
Now that we have the printed model of the teeth, the next step will be to go to the ceramics workshop to make the plaster/silica mix to enclose the 3D printed object. 

{{< figure src="week17/pouring.webp" caption="**Figure 9:** Pouring the plaster/silica mix [[source](https://www.instructables.com/Casting-Glass/)]" height="450">}}

After it dries, we will burn out the PLA from plaster/silica master mold (not sure where, perhaps also in the ceramics workshop? In some furnace.).

|    |    |    |
|----------|----------|----------|
|{{< figure src="week17/shards1.webp" caption="**Figure 10:** Breaking the class with hammer [[source](https://www.instructables.com/Casting-Glass/)]" height="450">}}| &nbsp; &nbsp; |{{< figure src="week17/shards2.webp" caption="**Figure 11:** Glass shards [[source](https://www.instructables.com/Casting-Glass/)]" height="450">}}

Then it will be time to go to the grass workshop and fill the mold with glass shards, put it in the kiln and "burn" it for a few days. I don't know how liquid will the glass get, I see that if it won't be liquid enough, then we might not get as fine feature as I would wish. I also don't really know how small will the broken glass be - I assume that if it's very small (like a powder) I can "pour" it directly into the teeth, but perhaps then there would be many bubbles? I don't know yet, and I'm curious to find out!

{{< figure src="week17/kiln.jpeg" caption="**Figure 12:** Glass in kiln [[source](https://www.thecrucible.org/guides/glass-casting/)]" height="450">}}

Then after this is done, we will break the master mold apart and post-process result in the cold workshop.

I found an interesting articles about the topic [here](https://www.thecrucible.org/guides/glass-casting/) and [here](https://www.instructables.com/Casting-Glass/). They used a wax instead a PLA print. That could be even more interesting, as I could perhaps sculpture the wax myself.

## Note

Seems like similar process can be used for aluminum casting! Maybe we can try that as well.
https://www.instructables.com/3D-Printed-Lost-PLA-Investment-Casting-Aluminium/

## Files
- Fusion360 models - [[.zip]](/digital-fabrication/files/glass-molding.zip)

<!-- 
minimal feature size 6mm
max dimensions of the mold?




Mesh MeshBody1

Body
Area	1.784E+06 mm^2
Density	0.008 g / mm^3
Mass	5.272E+05 g
Volume	6.716E+07 mm^3
Physical Material	Steel
Appearance	Steel - Satin

Mesh
Facet Count	42500
Vertex Count	21288
Face Group Count	1
Shell Count	19
Texture Count	0
Color	No

Bounding Box
	Length 	 1176.698 mm
	Width 	 779.022 mm
	Height 	 180.329 mm
Center of Mass	49.105 mm, 174.918 mm, -47.663 mm

Moment of Inertia at Center of Mass   (g mm^2)
	Ixx = 1.933E+10
	Ixy = 7.192E+08
	Ixz = 4.696E+07
	Iyx = 7.192E+08
	Iyy = 4.496E+10
	Iyz = -2.717E+08
	Izx = 4.696E+07
	Izy = -2.717E+08
	Izz = 6.328E+10

Moment of Inertia at Origin   (g mm^2)
	Ixx = 3.665E+10
	Ixy = -3.809E+09
	Ixz = 1.281E+09
	Iyx = -3.809E+09
	Iyy = 4.743E+10
	Iyz = 4.123E+09
	Izx = 1.281E+09
	Izy = 4.123E+09
	Izz = 8.068E+10

Mesh Analysis
Mesh is Closed	Yes
Mesh is Oriented	Yes
Mesh has Positive Volume	Yes -->