---
title: "About me"

---

Hi! This section is about me, what I do etc.

![Picture](/digital-fabrication/pic.png)

My name is Sara. I am a first-year master's student from Slovakia. Currently, I am studying Computer Science as my major, but I also did a bit of electrical engineering during my bachelor studies. The first time I got into prototyping was in the summer of 2021 when I was working on a research project in collaboration with the University of Victoria. The project was about designing, building and documenting an affordable DIY prototype of a syringe pump. After we finished the project, I became more interested in prototyping, so I took courses such as Design Thinking and Electronic Prototyping or Digital Fabrication. 