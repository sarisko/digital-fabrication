---
title: "Final Project - Robot"

---

For the final project, I hope to make a moving robot, ideally a self-balancing robot. A self-balancing robot is standing on two wheels only, and therefore needs to have a sensors (accelerometer, gyroscope) that would detect the movement and constantly adjust the robot's position to maintain stability.

{{< figure src="final_project/reference.jpg" caption="Reference picture, source: https://www.instructables.com/The-Breadboarded-Self-Balancing-Robot/" height="450">}}

## Background

You can find a wide rage of online tutorials on how to build a self-balancing robot. I will not be following a specific one, but instead I will be inspired by their ideas approaches, as I have access to a different equipment and components we have at the lab.

- https://www.instructables.com/The-Breadboarded-Self-Balancing-Robot/
- https://www.instructables.com/2-Wheeled-Self-Balancing-Robot/
- https://www.instructables.com/Arduino-Self-Balancing-Robot-1/
- https://www.instructables.com/GoodBoy-3D-Printed-Arduino-Robot-Dog/

### Phase one (MVP):
- the robot can move
- it is remotely controllable

{{< figure src="final_project/stage2.jpg" height="300">}}

### Phase two (add-ons):
- self-balancing
- the robot has a battery big enough to support it for some time

{{< figure src="final_project/stage1.jpg" height="300">}}

### Phase three (extra):
- make more than one version, different design
- make a robot with jointed legs instead wheels

{{< figure src="final_project/stage3.jpg" height="300">}}

## Components
### Electronics:
- [XIAO ESP32C](https://wiki.seeedstudio.com/XIAO_ESP32C3_Getting_Started/)
- [BLDC Motor](https://quartzcomponents.com/products/a2212-10t-13t-1400kv-bldc-motor)
- [Motor driver]()
- [Adafruit LSM6DS3TR-C 6-DoF Accel + Gyro](https://learn.adafruit.com/adafruit-lsm6ds3tr-c-6-dof-accel-gyro-imu/overview)
- ...

### Mechanical parts:
- ...

# Progress:

## Week 15:
This week I looked into components I would like to use, specifically the motors and accelerometers. To fulfill the minimal requirement, I only need the XIAO with antenna, two motors and a body with wheels. I decided to use the Adafruit accelerometer and gyroscope (in components) and Brushless motor.

## Week 17 and 18: 
Renamed the phase one robot to **lacar**.

I managed to move the brushless motors, I tested them at 10.5V - 11V. They needed 0.09A in rest and max 0.6A when rotating full speed.

{{< video src="final_project/motor_moving.mp4"  >}}

{{< video src="final_project/motor_voltage.mp4"  >}}

So the next step was to move the motors with batteries. I am using two cases, one case for the motors and one for powering the XIAO -> it should probably be around 5V as that's how much the motor driver needs according to [here](https://www.amazon.de/-/en/ICQUANZX-Brushless-Electric-Controller-Helicopter/dp/B081CWD3YG).

|    |    |    |
|----------|----------|----------|
|{{< figure src="final_project/battery_measure.jpg" caption="**Figure 1:** Ideal voltage from case of 4 batteries" height="450">}}| &nbsp; &nbsp; |{{< figure src="final_project/new_holder.gif" caption="**Figure 2:** Battery holder, design from Ariana" height="450">}}

I designed and made some mechanical parts such as wheels and wheel holders --> so the current design is super suboptimal and am aware of it, however the better design will come in next iteration because the priority is to actually move the "car". I am also not using super sophisticated design for the front wheel, so far there is just one wheel that can freely more 360 degrees (on Figure 9). The body is just a laser cut rectangle (for now).

|    |    |    |
|----------|----------|----------|
|{{< figure src="final_project/holder.jpg" caption="**Figure 3:** The motor holder" height="450">}}| &nbsp; &nbsp; |{{< figure src="final_project/wheel_sketch.jpg" caption="**Figure 4:** The wheel" height="450">}}

{{< figure src="final_project/wheel_mounted.jpg" caption="**Figure 5:** The wheel and holder together with the brushless motor and rest of the cat" height="450">}}

I also designed and made a board for the electronics, as it started to be pain to manage the cables. There were some error and warnings in the schematic design, because I was not sure how to properly note that the whole circuit is going to be battery powered. I managed to fix the errors, but I'm still not 100% whether that my schematic is correct.

|    |    |    |
|----------|----------|----------|
|{{< figure src="final_project/schematic.jpg" caption="**Figure 6:** Schematic design" height="450">}}| &nbsp; &nbsp; |{{< figure src="final_project/pcb_design.jpg" caption="**Figure 7:** PCB design" height="450">}}

At the end it turns out that I can't use the PCB anyway, because: 
- the holes for rivets were too small, and I'm worried that I scratched too much of the copper
- I didn't measure properly which wire to board terminal I need. I thought that the smaller one will be enough, but the cables to power the motor are too thick so I need the bigger one.

{{< figure src="final_project/bad_design.jpg" caption="**Figure 8:** Comparison in what I thought I need and what I actually need " height="450">}}

**Code:** So it turns out it is not that simple to control the brushless motors with ESP based board. So far I only powered the motors from the XIAO RP2040 using a Servo library, which doesn't support the ESP architecture. I found someone on the internet with a similar problem [here](https://esp32.com/viewtopic.php?t=20450). Only took him 3 days to find a workaround :') . So I am implementing that right now.

The code I am trying to debug right now:

{{< highlight go-html-template >}}
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>
#include <ESP32Servo.h>

Servo ESC;
Servo ESC2; 

// Some variables we will need along the way
const char* ssid     = "Fablab";
const char* password = "Fabricationlab1"; 
const char* PARAM_MESSAGE = "message"; 
int webServerPort = 80;
int pulse = D1;
int pulse2 = D2;

const char* html_response = R"(<!DOCTYPE html><html> <head> <title>Example</title> </head> <body> <!-- <p>This is an example of a simple HTML page with two buttons.</p> --> <input type="submit\" value=\"Turn on\" style=\"height:500px;width:100%;font-size:170px;\" onclick=\"window.location='/led_on';\" /> <div> </div> <input type=\"submit\" value=\"Turn off\" style=\"height:500px;width:100%;font-size:170px;\" onclick=\"window.location='/led_off';\" /></body></html>)"; 

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

void setup() {
  ESC.attach(pulse,1000,2000);
  ESC2.attach(pulse2,1000,2000);

  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "html", html_response);
  });

  server.on("/turn_on", HTTP_GET, [](AsyncWebServerRequest *request){
    ESC.write(100);
    ESC2.write(100);
  request->send(200, "html", html_response);
  });

  server.on("/turn_off", HTTP_GET, [](AsyncWebServerRequest *request){
   ESC.write(0);
   ESC2.write(0);
  request->send(200, "html", html_response);
  });

  // Usage IP_ADDRESS/led?state=1 where /led is our endpoint and ?state=on is a variable definition.
  // You can also chain variables like this: /led?sate=off&color=blue
  server.on("/led", HTTP_GET, [](AsyncWebServerRequest *request){
    bool state; // LED state 
    if (request->hasParam("state")) {
      // The incoming params are Strings
      String param = request->getParam("state")->value();
      // .. so we have to interpret or cast them
      state = ((param == "on") ? HIGH : LOW); // Look up Terary Operator (e.g. https://www.programiz.com/cpp-programming/ternary-operator)
    } else {
      state = LOW;
    }

    // Send back message to human
    String response = "Turning LED ";
    response += state ? "on" : "off"; 
    request->send(200, "text/plain", response);

    // Operate LED
    digitalWrite(led, state);
  });
  

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
} 

void loop() {
  // Nothing needed here at the moment
}
{{< /highlight >}}

So currently, lacar looks like this:
|    |    |    |
|----------|----------|----------|
|{{< figure src="final_project/lacar_angle.jpg" caption="**Figure 9:** The lacar at the angle" height="450">}}| &nbsp; &nbsp; |{{< figure src="final_project/lacar.jpg" caption="**Figure 10:** Lacar from above" height="450">}}


## Week 19

So the week started quite bad:
{{< youtube VIoDlJln46M >}}

Not only the brushless motors rotate way too fast, I am kinda wasting my time with them because they can only move one direction. I mean it't interesting, but I am stuck in this motor limbo for way too long.

So I got back to researching my options again. Other people mostly use the DC motors, especially the geared ones (I guess it gives the wheels more torque).

|    |    |    |
|----------|----------|----------|
|{{< figure src="final_project/yellow_motors.png" caption="**Figure 11:** Yellow geared motors" height="450">}}| &nbsp; &nbsp; |{{< figure src="final_project/motor_amazon.png" caption="**Figure 12:** Basic geared motors" height="450">}}

So I sound someone who made a nice tutorial explaining why he used which components, and he used the "yellow motors".

{{< figure src="final_project/robot_m.png" caption="**Figure 13:** Some other self-balancing car that I found ([source](https://circuitdigest.com/microcontroller-projects/arduino-based-self-balancing-robot))" height="450">}}

Turns out we also have them in the lab!! (hope they work). Yay. So for like the 3rd time, let's try these motors.

{{< figure src="final_project/lab_motor.jpg" caption="**Figure 14:** Turns out we have such motors at lab" height="450">}}

I also found some H-Bridge at home which is conveniently connected to power supply, so I can first debug it without worrying about the batteries.

{{< figure src="final_project/h_bridge.jpg" caption="**Figure 15:** H-bridge with power supply" height="450">}}

## Week 20 and further

After getting the geared motors, I tested them with the L298HN H-bridge.

{{<youtube x6pfowjRct0>}}

It worked as expected, so I started to assemble all together with the Xiao RP2040.

|    |    |    |
|----------|----------|----------|
|{{< figure src="final_project/xiao_test.jpg" caption="**Figure 16:** Test with Xiao RP2040" height="450">}}| &nbsp; &nbsp; |{{< figure src="final_project/xiao_test2.jpg" caption="**Figure 17:** Top view, robot with Xiao" height="450">}}

Soldered the connection between the bridge and the motors.

{{< figure src="final_project/wheels_taped.jpg" caption="**Figure 18:** Motors are covered by tape to protect the soldered connections" height="450">}}

However, the libraries I wanted to use for reading the accelerometer (MPU6050) data, and then feeding it to the PID algorithm were not compatible with the Xiao module. I was not sure what's wrong, so I tried to use them with some Arduino, and they worked!

{{< video src="final_project/s_no_audio.mp4" width="400">}}

So I then redid the setup with some smaller fake Arduino. There were some troubles because the Arduino was just some cheap copy (for example the board looked like Arduino Nano, had the same pinout as Arduino Nano, but then compiling the .cpp code, you have to select the Arduino Pro board).

{{< figure src="final_project/baud.jpg" caption="**Figure 19:** Getting weird characters because I was using wrong baud" height="450">}}

But at the end of the day, I managed to make it work also with the smaller Arduino.

{{< video src="final_project/no_audio.mp4" width="400">}}

Then it was time to make my own board with the motor drivers. I started by designing the board in KiCAD (files can be found at the end of this post). I was limited by size, and there were many components, so it wasn't possible to only use the one side of the board. It was my first time using double-sided design (and also doing double-sided milling), but luckily Saskia showed me how it's done.

|    |    |    |
|----------|----------|----------|
|{{< figure src="final_project/doublesided.jpg" caption="**Figure 20:** Milling the other side" height="450">}}| &nbsp; &nbsp; |{{< figure src="final_project/engr.jpg" caption="**Figure 21:** Need so select the layer #2 for the back side" height="450">}}

{{< figure src="final_project/gifko.gif" caption="**Figure 22:** Shiny freshly milled PCB board" height="450">}}

Then it was time to attach the electronic components. Kris showed me hot to properly attach the motor drivers using the heat gun, and also it is worth noticing that all the rivets are connected to the tracks with extra solder (I wouldn't know that this is important, but I once made board with rivets where I haven't done it, and the connections were awful! The whole board was a nightmare to debug).

|    |    |    |
|----------|----------|----------|
|{{< figure src="final_project/bridge_check.jpg" caption="**Figure 23:** Soldering H-bridge - closeup" height="450">}}| &nbsp; &nbsp; |{{< figure src="final_project/solderedh.jpg" caption="**Figure 24:** Soldering H-bridge connections" height="450">}}

Unfortunately, after doing all that I found out that I had a mistake in KiCAD - I was using the basic pin headers for Arduino Nano, and some of them got rotated in between schematic and PCB design -> I didn't know that this could happen, but will be definitely more careful with it from now on. So I had to mill again.

{{< figure src="final_project/thin.jpg" caption="**Figure 25:** Badly milled board - thin connections" height="450">}}

After milling, I noticed that some back side tracks of this mill were very very thin. As the tracks get thinner, the resistance goes up, so I was worried that there is going to be an inconsistency in the motor responses. Such a shame, the front side was super nice. Another mill in necessary!

{{< figure src="final_project/ruined3.jpg" caption="**Figure 26:** Badly milled board - not deep enough" height="450">}}

The last mill I made was also not good because some paths were milled too shallow, and therefore the connections were not separated.

The rest is TBD! :)

{{<youtube PLFCy-uT7rQ>}}
Working Sara.

## Files
- Robot body STL - [[.zip]](/digital-fabrication/files/robot_body.zip)  --> [souce](https://circuitdigest.com/microcontroller-projects/arduino-based-self-balancing-robot) 
- KiCAD design - [[.zip]](/digital-fabrication/files/kicad_final.zip)

## Final project slide
{{< figure src="presentation.png"  height="450">}}

## Final project video
{{< video src="presentation.mp4" width="400">}}

<!-- 
For my final project, I am thinking of making smart furniture of some kind. Currently, I am not sure whether it would be a cabinet or something else, but I explored a nightstand option. 

![Idea](/digital-fabrication/project_idea.png)

As seen in the sketch above, the purpose would be to have a nice, aesthetically pleasing nightstand that would offer some extra functionality, for example, built-in wireless charging, and some interesting light (perhaps Philips Hue?). A more advanced feature could be a simple medicine dispenser - I have a medicine that I have to take every morning, but sometimes I forget whether I already took it that day or not. There could be a mechanism that would open the box with medicine in the morning, and if I take something out of it, it will close for the day (open it again the next morning). That way the user (in this case me) would know whether I already took the medicine or not. 

Links:
https://arduino.stackexchange.com/questions/76708/power-supply-for-seeeduino-xiao



-->

<!-- 
Answered in the final project page:
- wht will it do
- what will you design
- what parts and systems will be made
- what processes will be used
- I used a 3D printing, PCB milling, soldering and 

- have the files sharable

 -->
