---
title: "Introduction"

---

## Introduction

This website is a diary that describes my journey through the 2023 edition of the Aalto Digital Fabrication course. The course follows the global Fab Academy curriculum, where students learn rapid prototyping by planning and executing a new project each week.

## Resources
Some useful links include: 
- [Global academy schedule](http://fabacademy.org/2023/)
- [What is Fab Academy](http://fabacademy.org/)
- [My repository](https://gitlab.com/sarisko/digital-fabrication)
