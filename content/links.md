---
title: "Links"
---
## Important and other
- [FabLab wiki](https://wiki.aalto.fi/display/AF/Aalto+Fablab+Home)
- [Open Lab Starter Kit](https://www.inmachines.net/open-lab-starter-kit) - An open source set of machines for Fab Labs and Open Labs
- [Aalto Studios](https://studios.aalto.fi/fablab/)
- [List of student pages](https://gitlab.com/aaltofablab/digital-fabrication-2023)
## Week 1 links
-  Open source [game](https://ohmygit.org/) about git
- Visual representation of [git](https://initialcommit.com/blog/git-sim)
- Booking system to machine introduction sessions [link](https://outlook.office365.com/owa/calendar/AaltoFablabOrientation@aaltofi.onmicrosoft.com/bookings/)
- Example FabAcademy documentation [here](https://fabacademy.org/2021/labs/waag/students/nadieh-bremer/blog/week-2/) - impressive
- FabAcademy learning content - https://academy.cba.mit.edu/classes/
## Week 2 links
- FabAcademy [song](https://www.youtube.com/watch?v=tV6nzcfG2CA)
## Week 3 links
- [Icons](https://thenounproject.com/icon/3d-printer-2090701/) used on this page made by Hans Gerhard Meier
- [kdenlive](kdenlive.org) - open source and free video editor
- [ImageMagic](https://imagemagick.org/) - for image compression and other things
- [MLT Framework](https://www.mltframework.org/) - open source multimedia framework, designed and developed for television broadcasting. Can be used for video editing.
## Week 4 links
- [OpenSCAD cheat sheet](https://openscad.org/cheatsheet/index.html)
- [Fun tool CAD sculptureish](https://stephaneginier.com/sculptgl/)
- https://theresanaiforthat.com/
- [SolveSpace](https://www.youtube.com/playlist?list=PLGAjLwYQPgaBafzQTLA84IkTOptOdIsUX) beginner challenges
- Kris introduction to SolveSpace [video](https://www.youtube.com/watch?v=8JaQCG9PO2c&feature=youtu.be)
- [Formlabs Software] (https://formlabs.com/software/) - for thr SLA 3D printing
## Week 5 links
- Group Assignment [repository](https://gitlab.com/aaltofablab/laser-cutting-2023-group-a) - GroupA
- [Laser Cutting introduction](https://aaltofi-my.sharepoint.com/personal/krisjanis_rijnieks_aalto_fi/_layouts/15/onedrive.aspx?csf=1&web=1&e=TIihgb&cid=5d3d4e3b%2Dd91f%2D4736%2Da2b5%2D2a60ecb81b54&id=%2Fpersonal%2Fkrisjanis%5Frijnieks%5Faalto%5Ffi%2FDocuments%2FDigital%20Fabrication%202023%2FShared%2F2023%2D02%2D09%5FComputerControlledCutting&FolderCTID=0x0120004C97616E91C6B7429A5E329EB2DED611)
- [Vinyl cutting FabLab Wiki](https://wiki.aalto.fi/display/AF/Vinyl+Cutting+with+Roland+GX-24)

## Week 8 links
- [Fab electronics component library for KiCad](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad)