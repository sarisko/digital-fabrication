# Digital Fabrication

This repository is a documentation of my progress in the 2023 edition of the Digital Fabrication couse that is based on the Fab Academy. 
Clicke [here](https://sarisko.gitlab.io/digital-fabrication/) to access my website.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
